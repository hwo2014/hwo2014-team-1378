package noobbot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Map.Entry;

public class CurveProps {
	public static class CurveKey {
		double radius;
		double angle;
		boolean crashed;

		static CurveKey fromString(final String s) {
			CurveKey key = new CurveKey();
			String[] parts = s.split("_");
			key.radius = Double.parseDouble(parts[1]);
			key.angle = Math.abs(Double.parseDouble(parts[2]));
			key.crashed = Boolean.parseBoolean(parts[3]);
			return key;
		}

		@Override
		public String toString() {
			return "CURVE_" + radius + "_" + Math.abs(angle) + "_" + crashed;
		}
	}

	//	public static class CurveValue {
	//		double currentSpeed;
	//		double throttle;
	//
	//		static CurveValue fromString(final String s) {
	//			if (s == null || "".equals(s)) {
	//				return null;
	//			}
	//			CurveValue cv = new CurveValue();
	//			String[] parts = s.split("_");
	//			cv.currentSpeed = Double.parseDouble(parts[0]);
	//			cv.throttle = Double.parseDouble(parts[1]);
	//			return cv;
	//		}
	//
	//		@Override
	//		public String toString() {
	//			return currentSpeed + "_" + throttle;
	//		}
	//	}

	private static Map<String, Integer> curveValues = new HashMap<>();
	private static String mapname;

	public static void setMapname(final String mapname) {
		CurveProps.mapname = mapname + "-curves.properties";
	}

	public static void load() {
		//		Properties p = new Properties();
		//		try {
		//			p.load(new FileInputStream(new File(mapname)));
		//
		//			for (Entry entry : p.entrySet()) {
		//				String fieldname = entry.getKey().toString();
		//				if (fieldname.startsWith("CURVE_")) {
		//					curveValues.put(fieldname, Integer.parseInt(entry.getValue().toString()));
		//				}
		//
		//			}
		//
		//			Thread.sleep(1); // 1000
		//
		//		} catch (FileNotFoundException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}

	}

	public static void save() {
		//		Properties p = new Properties();
		//
		//		for (Entry<String, Integer> entry : curveValues.entrySet()) {
		//			p.put(entry.getKey().toString(), entry.getValue().toString());
		//		}
		//
		//		try {
		//			p.store(new FileWriter(new File(mapname)), "");
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
	}

	public static Map<String, Integer> getCurveValues() {
		return curveValues;
	}
}
