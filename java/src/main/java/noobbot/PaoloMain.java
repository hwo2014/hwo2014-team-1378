package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.Brain.Entry;
import noobbot.CarPosition;
import noobbot.GameInit;
import noobbot.MsgHelper.*;

import com.google.gson.Gson;

public class PaoloMain {
	public static void main(final String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];
		String trackName = "usa";
		String password = "ThisIsNotForFun";

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		// new Main(reader, writer, new Join(botName, botKey)); String botName,
		// String key, String trackName, String password, int carcount
		new PaoloMain(reader, writer, new CreateRace(botName, botKey, trackName, password, 1));
		socket.close();
	}

	final Gson gson = new Gson();
	private PrintWriter writer;
	private double speed = 0.5;
	private double oldAngle = 0;
	private double oldDistance = 0;
	private double trueSpeed = 0;
	private int lastPiecePos;
	private GameInit gameInit;

	// public Main(final BufferedReader reader, final PrintWriter writer, final
	// Join join) throws IOException {
	public PaoloMain(final BufferedReader reader, final PrintWriter writer, final CreateRace create) throws IOException {
		this.writer = writer;
		boolean turbo = false;
		String line = null;

		send(create);
		System.out.println(create.toJson());
		System.out.println("create sent");

		JoinRace joinRace = new JoinRace(create.botId.name, create.botId.key, create.trackName, create.password, create.carCount);

		send(joinRace);
		System.out.println("joinRace sent");
		System.out.println(joinRace.toJson());

		while ((line = reader.readLine()) != null) {
			try {
				final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

				if (msgFromServer.msgType.equals("carPositions")) {
					final CarPositions carPositions = gson.fromJson(line, CarPositions.class);

					CarPosition carPosition = carPositions.data[0];
					if (carPositions.data.length > 1) {
						System.out.println("LONGER!");
					}
					lastPiecePos = carPosition.piecePosition.pieceIndex;
					int nextPiecePos = lastPiecePos + 1;
					int nextnextPiecePos = lastPiecePos + 2;
					int pieces = gameInit.data.race.track.pieces.length;
					if (nextPiecePos >= pieces) {
						nextPiecePos = 0;
					}
					if (nextnextPiecePos >= pieces) {
						nextnextPiecePos = nextnextPiecePos - pieces;
					}
					/*
					 * if (carPosition.angle>30 || carPosition.angle<-30) {
					 * speed=speed-0.1; }
					 */
					int carAngle = 40;
					if (oldDistance>carPosition.piecePosition.inPieceDistance) {
						trueSpeed=carPosition.piecePosition.inPieceDistance-0;
					} else {
						trueSpeed=carPosition.piecePosition.inPieceDistance-oldDistance;
					}
					if (Math.abs(carPosition.angle) > carAngle) {
						if (gameInit.data.race.track.pieces[nextPiecePos].angle != 0
								|| gameInit.data.race.track.pieces[lastPiecePos].angle != 0) {
							if ((Math.abs(oldAngle) > 0 && Math.abs(oldAngle) >= Math.abs(carPosition.angle))) {
								speed = speed + 0.1;
							} else {
								speed = speed - 0.1;
								if (speed > 0.5 && gameInit.data.race.track.pieces[lastPiecePos].angle == 0) {
									speed -= 0.1;
								}
								if (gameInit.data.race.track.pieces[nextPiecePos].angle == 0) {
									speed += 1;
									if (turbo) { //&& gameInit.data.race.track.pieces[nextnextPiecePos].angle==0) {
										speed = 0;
										writer.println(MsgHelper.turbo);
										System.out.println(MsgHelper.turbo);
										turbo=false;
									}
								}
							}
						}
					} else if (gameInit.data.race.track.pieces[nextPiecePos].angle == 0) {
						if (gameInit.data.race.track.pieces[nextnextPiecePos].angle != 0) {
							speed = speed + 0.1;
						} else {
							speed = speed + 1;
							if (turbo && gameInit.data.race.track.pieces[nextnextPiecePos].angle==0) {
								speed = 0;
								writer.println(MsgHelper.turbo);
								System.out.println(MsgHelper.turbo);
								turbo=false;
							}
						}
					} else if (gameInit.data.race.track.pieces[nextPiecePos].angle != 0) {
						if (gameInit.data.race.track.pieces[lastPiecePos].angle != 0) {
							speed = speed + 0.2;
							System.out.println("Turn faster!");
						} else if (gameInit.data.race.track.pieces[lastPiecePos].angle == 0) {
							speed = speed - 0.1;
							System.out.println("Braking Hard!");
						}
					} else {
						speed += 0.4;
					}
					/*
					 * if
					 * (gameInit.data.race.track.pieces[nextPiecePos].angle>0) {
					 * speed=speed-0.4; }
					 */
					if (speed < 0.3) {
						speed = 0.3;
					}
					if (speed > 1) {
						speed = 1;
					}

					Entry thisEntry = Brain.entries.get(lastPiecePos);
					Entry nextEntry = Brain.entries.get(nextPiecePos);
					Entry nextnextEntry = Brain.entries.get(nextnextPiecePos);

					// TODO bösartigkeit der kurve errechnen
					// TODO gefahrene distanz errechnen
					// if (gameInit.data.race.track.pieces[lastPiecePos].angle >
					// 30.
					// || gameInit.data.race.track.pieces[nextPiecePos].angle >
					// 30.) {
					// speed = .6;
					// } else
					/*
					 * if (thisEntry != null && thisEntry.crashed) { speed = .1;
					 * } else if (nextEntry != null && nextEntry.crashed) {
					 * speed = .4; } else if (nextnextEntry != null &&
					 * nextnextEntry.crashed) { speed = .5; } else if
					 * (Math.abs(carPosition.angle) > 45.) { speed = .1; } else
					 * if (Math.abs(carPosition.angle) > 10.) { speed = .5; }
					 * else if (Math.abs(carPosition.angle) > 5.) { speed -= .1;
					 * speed = Math.max(0.5, speed); } else { //speed += .1;
					 * speed = Math.min(1.0, speed); }
					 * 
					 * speed = Math.max(0.3, speed);
					 */
					oldAngle=carPosition.angle;
						oldDistance=carPosition.piecePosition.inPieceDistance;
					send(new Throttle(speed));
					// System.out.println("New speed is: "+speed);

				} else if (msgFromServer.msgType.equals("crash")) {
					Entry entry = new Entry();
					entry.crashed = true;
					Brain.entries.put(lastPiecePos, entry);
					System.out.println("Crashed at position " + lastPiecePos);
					System.out.println("Speed was: " + trueSpeed);

				} else if (msgFromServer.msgType.equals("join")) {
					System.out.println("Joined");
				} else if (msgFromServer.msgType.equals("gameInit")) {
					System.out.println("Race init");
					gameInit = gson.fromJson(line, GameInit.class);
					System.out.println(line);
				} else if (msgFromServer.msgType.equals("lapFinished")) {
					System.out.println("Lap Finished");
					System.out.println("LAP FINISHED: " + line);
					final LapFinished lapFinished = gson.fromJson(line, LapFinished.class);
				} else if (msgFromServer.msgType.equals("dnf")) {
					System.out.println("dnf");
				} else if (msgFromServer.msgType.equals("gameEnd")) {
					System.out.println("Race end");
				} else if (msgFromServer.msgType.equals("gameStart")) {
					System.out.println("Race start");
				} else if (msgFromServer.msgType.equals("turboAvailable")) {
					System.out.println("Turbo Available!");
					final TurboAvailable turboAvailable = gson.fromJson(line, TurboAvailable.class);
					System.out.println(line);
					turbo = true;
				} else {
					System.out.println(line);
					send(new Ping());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}
