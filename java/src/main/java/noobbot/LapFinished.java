package noobbot;

import noobbot.GameInit.Data;

public class LapFinished {
	
	public String msgType;
	Data data;
	String gameId;
	int gameTick;
	
	public LapFinished() {
		// TODO Auto-generated constructor stub
	}

	public LapFinished(final String msgType, final Data data) {
		this.msgType = msgType;
		this.data = data;
	}
	
	public class Data {
		Car car;
		LapTime lapTime;
		RaceTime raceTime;
		Ranking ranking;
	}
	
	public class Car {
		String name;
		String color;
	}
	
	public class LapTime {
		int lap;
		int ticks;
		int millis;
	}
	
	public class RaceTime {
		int laps;
		int ticks;
		int millis;
	}
	
	public class Ranking {
		int overall;
		int fastestLap;
	}
}