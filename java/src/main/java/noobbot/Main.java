package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;

import noobbot.Brain.Entry;
import noobbot.CarPosition;
import noobbot.GameInit;
import noobbot.GameInit.Piece;

import com.google.gson.Gson;

public class Main {
	public static void main(final String... args) throws IOException {
		KlausMain.main(args);
		//		PaoloMain.main(args);
	}
}


abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	// public String gameId;
	// public int gameTick;

	MsgWrapper(final String msgType, final Object data, final String gameId,
				final int gameTick) {
		this.msgType = msgType;
		this.data = data;
		// this.gameId = gameId;
		// this.gameTick = gameTick;
	}

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class CarPositions {
	public String msgType;
	// public String gameId;
	// public int gameTick;
	CarPosition[] data;

	String gameId;
	Integer gameTick;

	public CarPositions() {}

	public CarPositions(final String msgType, final CarPosition[] data,
						final String gameId, final int gameTick) {
		this.msgType = msgType;
		this.data = data;
		// this.gameId = gameId;
		// this.gameTick = gameTick;
	}

}


class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}


class SwitchLane extends SendMsg {
	private String value;

	public SwitchLane(final boolean left) {
		if (left) {
			value = "Left";
		} else {
			value = "Right";
		}
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}
class Throttle extends SendMsg {
	private double value;

	public Throttle(final double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}
