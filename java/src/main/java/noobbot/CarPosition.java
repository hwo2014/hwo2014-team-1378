package noobbot;

public class CarPosition {
	static class Id {
		String name;
		String color;
	}

	static class Lane {
		int startLaneIndex;
		int endLaneIndex;
	}

	static class PiecePosition {
		int pieceIndex;
		double inPieceDistance;
		int lap;
		Lane lane;
	}

	Id id;
	double angle;
	PiecePosition piecePosition;

}
