package noobbot;

public class MsgHelper {

	public class Crash {

		public String msgType;
		Data data;
		String gameId;
		int gameTick;

		public Crash() {

		}

		public Crash(final String msgType, final Data data) {
			this.msgType = msgType;
			this.data = data;
		}

		public class Data {
			String name;
			String color;
		}
	}

	public class Spawn {

		public String msgType;
		Data data;
		String gameId;
		int gameTick;

		public Spawn() {

		}

		public Spawn(final String msgType, final Data data) {
			this.msgType = msgType;
			this.data = data;
		}

		public class Data {
			String name;
			String color;
		}
	}

	public class Dnf {

		public String msgType;
		Data data;
		String gameId;
		int gameTick;

		public Dnf() {

		}

		public Dnf(final String msgType, final Data data) {
			this.msgType = msgType;
			this.data = data;
		}

		public class Data {
			Car car;
			String reason;
		}

		public class Car {
			String name;
			String color;
		}
	}

	public class Finish {

		public String msgType;
		Data data;
		String gameId;
		int gameTick;

		public Finish() {

		}

		public Finish(final String msgType, final Data data) {
			this.msgType = msgType;
			this.data = data;
		}

		public class Data {
			String name;
			String color;
		}
	}

	public static class JoinRace extends SendMsg {
		BotId botId;
		String trackName;
		String password;
		int carCount;

		public class BotId {
			String name;
			String key;
		}

		public JoinRace(final String botName, final String key, final String trackName,
				final String password, final int carcount) {
			this.botId = new BotId();
			this.botId.name = botName;
			this.botId.key = key;
			this.trackName = trackName;
			this.password = password;
			this.carCount = carcount;
		}

		@Override
		protected String msgType() {
			return "joinRace";
		}
	}

	public static class CreateRace extends SendMsg {
		BotId botId;
		String trackName;
		String password;
		int carCount;

		public class BotId {
			String name;
			String key;
		}

		public CreateRace(final String botName, final String key, final String trackName,
				final String password, final int carcount) {
			this.botId = new BotId();
			this.botId.name = botName;
			this.botId.key = key;
			this.trackName = trackName;
			this.password = password;
			this.carCount = carcount;
		}
		
		@Override
		protected String msgType() {
			return "createRace";
		}
	}
	
	public class TurboAvailable {
		public String msgType;
		Data data;

		class Data {
			double turboDurationMilliseconds;
			double turboDurationTicks;
			double turboFactor;
		}
	}
	
	public static String turbo = "{\"msgType\": \"turbo\", \"data\": \"Gogo Power Rangers!\"}";
	
}
