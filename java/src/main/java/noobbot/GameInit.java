package noobbot;

import com.google.gson.annotations.SerializedName;

public class GameInit {

	public String msgType;
	Data data;

	public GameInit() {
		// TODO Auto-generated constructor stub
	}

	public GameInit(final String msgType, final Data data) {
		this.msgType = msgType;
		this.data = data;
	}

	static class Data {
		Race race;
	}

	static class Race {
		Track track;
		Car[] cars;
		RaceSession racesession;
	}

	static class Track {
		String id;
		String name;
		Piece[] pieces;
		Lane[] lanes;
		StartingPoint startingPoint;

		/** calculated. */
		double length;

	}

	static class Piece {

		double length;
		@SerializedName("switch")
		boolean sw;
		double angle;
		double radius;

		/** calculated or set. */
		double laneLength[];
		double lengthFromStart[];
		double targetSpeed;
		double laneTargetSpeed[];
		int targetLane;
		double curvature;
		double risk;
		double anglef;
		Piece next;
		Piece last;
		int id;

		//		double calculateCurvature() {
		//			if (radius == 0.) {
		//				curvature = 0.;
		//				return 0.;
		//			}
		//			curvature = 100. / radius; // radius == 100 typically
		//			risk = curvature * Math.abs(angle) * SetProperties.CURVE_RISK_FACTOR;
		//			return curvature;
		//		}
		//
		//		void setTargetSpeed(final double t) {
		//			double diff = t - targetSpeed;
		//			addTargetSpeed(diff / 2);
		//		}
		//
		void addTargetSpeed(final double t) {
			if (targetSpeed + t < 99.) {
				targetSpeed += t;
				targetSpeed = Math.max(targetSpeed, 1.);
				if (Math.abs(t) > .1) {
					last.addTargetSpeed(t / 2);
				}
			}
		}

	}

	static class Lane {
		int distanceFromCenter;
		int index;
	}

	static class StartingPoint {
		Position[] positions;
		double angle;
	}

	static class Position {
		double x;
		double y;
	}

	static class Car {
		Id id;
		Dimension dimensions;
	}

	static class Id {
		String name;
		String color;
	}

	static class Dimension {
		double length;
		double width;
		double guideFlagPosition;
	}

	static class RaceSession {
		int laps;
		int maxLapTimeMs;
		boolean quickRace;
	}

	public static double getCurveTargetSpeed(final double angle, final double radius) {
		double curv = 1. / radius * Math.abs(angle);
		return sl(SetProperties.CURVE_V_INIT_PARAM - Math
			.pow((curv + SetProperties.CURVE_V_INIT_PARAM2), SetProperties.CURVE_V_INIT_PARAM3));
	}

	public void init() {
		for (int i = 0; i < data.race.track.pieces.length; i++) {
			Piece p = data.race.track.pieces[i];
			//			p.lengthFromStart = ;
			p.id = i;

			if (i < data.race.track.pieces.length - 1) {
				p.next = data.race.track.pieces[i + 1];
			} else {
				p.next = data.race.track.pieces[0];
			}
			if (i > 0) {
				p.last = data.race.track.pieces[i - 1];
			} else {
				p.last = data.race.track.pieces[data.race.track.pieces.length - 1];
			}

			//			if (Math.abs(p.angle) > 30.) {
			//				p.targetSpeed = 4.; //2
			//			} else {
			//				p.targetSpeed = 7.; //5
			//			}
			//			if (Math.abs(p.angle) > 1.) {
			//				double curvature = p.calculateCurvature();
			//				p.targetSpeed = 4.95 * curvature; //2
			//			} else {
			//				if (p.next.calculateCurvature() > .4) {
			//					p.targetSpeed = 5.5; //5
			//				} else if (p.next.angle == 0f && p.next.angle == 0f) {
			//					p.targetSpeed = 11.; //5
			//				} else if (p.next.angle == 0f) {
			//					p.targetSpeed = 10.; //5
			//				}
			//			}
			p.laneLength = new double[data.race.track.lanes.length];
			if (Math.abs(p.length) < 1.) {
				// Kreisbogen (http://de.wikipedia.org/wiki/Kreisbogen)
				double l = Math.PI * p.radius * Math.abs(p.angle) / 180.;
				p.length = l;
				p.laneTargetSpeed = new double[data.race.track.lanes.length];
				for (int j = 0; j < data.race.track.lanes.length; j++) {
					double ll;
					if (p.angle < -0.1) {
						// distanceFromCenter is distance to the right from the center
						ll = Math.PI * (p.radius + data.race.track.lanes[j].distanceFromCenter) * Math.abs(p.angle) / 180.;
					} else if (p.angle > 0.1) {
						ll = Math.PI * (p.radius - data.race.track.lanes[j].distanceFromCenter) * Math.abs(p.angle) / 180.;
					} else {
						// is straight!
						ll = p.length;
					}
					p.laneLength[j] = ll;
				}
			} else {
				for (int j = 0; j < data.race.track.lanes.length; j++) {
					p.laneLength[j] = p.length;
				}
			}
		}
		for (int i = 0; i < data.race.track.pieces.length; i++) {
			Piece p = data.race.track.pieces[i];
			p.lengthFromStart = new double[data.race.track.lanes.length];
			if (i > 0) {
				for (int j = 0; j < data.race.track.lanes.length; j++) {
					p.lengthFromStart[j] = p.last.lengthFromStart[j] + p.last.laneLength[j];
				}
			}
		}
		data.race.track.length = data.race.track.pieces[data.race.track.pieces.length - 1].lengthFromStart[0];

		double anglef = 0.;

		int maxlane = data.race.track.lanes.length - 1;
		int lane0 = data.race.track.lanes[0].distanceFromCenter > 0 ? maxlane : 0;
		int lane1 = data.race.track.lanes[data.race.track.lanes.length - 1].distanceFromCenter > 0 ? maxlane : 0;

		// calculate lanes
		int lastLane = -1;
		for (int i = data.race.track.pieces.length * 2; i > 0; i--) {
			Piece p;
			if (i > data.race.track.pieces.length) {
				p = data.race.track.pieces[i - 1 - data.race.track.pieces.length];
			} else {
				p = data.race.track.pieces[i - 1];
			}
			//			p.targetLane = lane;

			p.targetLane = lastLane;
			if (p.sw) {
				if (anglef < -0.01) {
					p.targetLane = lane0;
				} else if (anglef > 0.01) {
					p.targetLane = lane1;
				} else {
					//					p.targetLane = -1;
				}
				lastLane = p.targetLane;
				// if piece has switch
				anglef = 0.;
			} else {
				//				p.targetLane = -1;
			}
			if (Math.abs(p.angle) > .1) {
				anglef = anglef + ((1. / p.radius) * p.angle);
			}
			p.anglef = anglef;

		}

		for (int i = 0; i < data.race.track.pieces.length; i++) {
			Piece p = data.race.track.pieces[i];
			double dist = 1.;
			double risk = 0.;
			// look distance in the future of the track
			//			Piece pp = p;
			//			double max_distance = SetProperties.MAX_DISTANCE_FOR_RISK_CALCULATION;
			//			while (dist < max_distance) {
			//				dist += pp.length;
			//				// do risk computation
			//				double distance_risk_minification_factor = SetProperties.DISTANCE_RISK_MINIFICATION_FACTOR;
			//				risk += (pp.risk / (dist * distance_risk_minification_factor));
			//
			//				pp = pp.next;
			//			}

			//			p.targetSpeed = SetProperties.MAX_VELOCITY - risk;
			//			p.targetSpeed = Math.max(p.targetSpeed, SetProperties.MIN_VELOCITY);
			//			if (p.targetSpeed != p.targetSpeed) {
			//				// targetSpeed is NaN
			//				p.targetSpeed = 5.;
			//			}

			if (Math.abs(p.angle) > .1) {
				p.targetSpeed = getCurveTargetSpeed(p.angle, p.radius);

				for (int j = 0; j < data.race.track.lanes.length; j++) {
					double radius = 0.;
					if (p.angle < -0.1) {
						// distanceFromCenter is distance to the right from the center
						radius = (p.radius + data.race.track.lanes[j].distanceFromCenter);
					} else if (p.angle > 0.1) {
						radius = (p.radius - data.race.track.lanes[j].distanceFromCenter);
					}
					p.laneTargetSpeed[j] = getCurveTargetSpeed(p.angle, radius);
				}
			} else {
				p.laneTargetSpeed = new double[data.race.track.lanes.length];
				for (int j = 0; j < data.race.track.lanes.length; j++) {
					p.laneTargetSpeed[j] = sl(p.targetSpeed);
				}
			}

			//			String s =
			//					String
			//						.format("id: %3d  angle: %3.0f  radius: %3.0f  length: %3.0f  risk: %6.1f  targetSpeed: %5.2f  targetLane: %1d  ",
			//								i, p.angle,
			//								p.radius, p.length, risk,
			//								p.targetSpeed, p.targetLane);
			//			KlausMain.log(s);
		}

		calculateMaxV();

		for (int i = 0; i < data.race.track.pieces.length; i++) {
			Piece p = data.race.track.pieces[i];
			String s =
					String
						.format("id: %3d  angle: %3.0f  radius: %3.0f  l: %3.0f l1: %3.0f l2: %3.0f risk: %6.1f  tV: %5.2f tV1: %5.2f tV2: %5.2f  targetLane: %1d  ",
								i, p.angle,
								p.radius, p.length, p.laneLength[0], p.laneLength[1], p.risk,
								p.targetSpeed, p.laneTargetSpeed[0], p.laneTargetSpeed[1], p.targetLane);
			KlausMain.log(s);
		}

		// data.race.track.pieces
	}

	/**
	 * calc max v from curve's max v.
	 */
	public void calculateMaxV() {
		// calculate max V
		int lanes = data.race.track.lanes.length;
		double maxV[] = new double[lanes];
		for (int j = 0; j < lanes; j++) {
			maxV[j] = 100.;
		}

		for (int j = 0; j < data.race.track.lanes.length; j++) {
			boolean first = false;
			for (int i = data.race.track.pieces.length * 2; i > 0; i--) {
				Piece p;
				if (i > data.race.track.pieces.length) {
					p = data.race.track.pieces[i - 1 - data.race.track.pieces.length];
				} else {
					p = data.race.track.pieces[i - 1];
				}

				if (Math.abs(p.angle) > 1.) {
					double targetSpeed = p.laneTargetSpeed[j];
					if (targetSpeed < maxV[j]) {
						maxV[j] = p.laneTargetSpeed[j];
					}
					first = true;
				} else {
					if (first) {
						p.laneTargetSpeed[j] = sl(maxV[j]);
						first = false;
					} else {
						double length = p.length;
						while (length > 0.) {
							length -= maxV[j];
							// get previous velocity when we brake (throttle = 0)

							maxV[j] = KlausMain.getVprev(maxV[j], 0.);
						}
						p.laneTargetSpeed[j] = sl(maxV[j]);
					}
				}
			}
		}
	}

	public static double sl(final double speed) {
		return Math.max(2., speed);
	}
}
