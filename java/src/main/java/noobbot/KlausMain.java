package noobbot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import noobbot.GameInit.Piece;
import noobbot.MsgHelper.CreateRace;
import noobbot.MsgHelper.JoinRace;
import noobbot.MsgHelper.TurboAvailable;
import noobbot.CurveProps.CurveKey;

import com.google.gson.Gson;

public class KlausMain {

	private static final double CURVE_MAX_AVG_ANGLE = 14.;

	static class KThrottle {
		public String msgType = "throttle";
		public double data;
		public int gameTick;

		public KThrottle(final double value, final int gametick) {
			this.data = value;
			this.gameTick = gametick;
		}

	}

	public static class CurveHistoryEntry {
		double currentSpeed;
		double throttle;
		double position;
		int lane;
		double angle;
		int inPieceGametick;
		private int gametick;

		public CurveHistoryEntry(final double currentSpeed, final double throttle, final double position, final int lane,
									final double angle, final int inPiecegametick, final int gametick) {
			super();
			this.currentSpeed = currentSpeed;
			this.throttle = throttle;
			this.position = position;
			this.lane = lane;
			this.angle = angle;
			this.inPieceGametick = inPiecegametick;
			this.gametick = gametick;
		}

	}

	public static class EnemyPos {
		int lane;
		double inPieceDistance;
		int pieceId;
		double lastPos;
		double currentSpeed;

		public EnemyPos(final int lane, final double inPieceDistance, final int pieceId) {
			super();
			this.lane = lane;
			this.inPieceDistance = inPieceDistance;
			this.pieceId = pieceId;
		}

	}

	Map<String, EnemyPos> enemies = new HashMap<>();

	private static boolean noclean = true;

	public static DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

	public static String turboLine = "{\"msgType\": \"turbo\", \"data\": \"Gogo Power Rangers!\"}";
	boolean turbo = false;

	Set<CurveHistoryEntry> curveHistory = new HashSet<>();

	public static void main(final String... args) throws IOException {
		System.out.println("ARGS: ");
		for (String string : args) {
			System.out.println("* " + string);
		}

		try {

			String host = args[0];
			int port = Integer.parseInt(args[1]);
			botName = args[2];
			String botKey = args[3];
			String trackName = args[4];
			if (trackName.equalsIgnoreCase("Racing")) {
				trackName = null;
				ciGame = true;
			}

			// TODO always set ciGame to true now:
			ciGame = true;

			//			String trackName = "germany";
			String password = "";
			int carCount = 1;

			//			filewriter = new FileWriter(new File("log/" + trackName + "-" + System.currentTimeMillis()));
			//			curvefilewriter = new FileWriter(new File("straights.csv"), true);
			log("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			final Socket socket = new Socket(host, port);
			final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

			SetProperties.setMapname(trackName);
			SetProperties.load();

			CurveProps.setMapname(trackName);
			CurveProps.load();

			SendMsg createRace = null;
			if (trackName != null && !"".equals(trackName)) {
				createRace = new CreateRace(botName, botKey, trackName, password, carCount);
			} else {
				createRace = new Join(botName, botKey);
			}

			System.out.println("Starting...");
			new KlausMain(reader, writer, createRace);

			socket.close();
		} finally {
			//			if (filewriter != null) {
			//				filewriter.close();
			//			}
			//			if (curvefilewriter != null) {
			//				curvefilewriter.close();
			//			}
		}
	}

	final Gson gson = new Gson();
	private PrintWriter writer;

	private static boolean ciGame = false;

	/**
	 * The throttle we will give.
	 */
	private double throttle = 1.0;

	/**
	 * Index of last piece we were on.
	 */
	private int currentPiecePos;

	/**
	 * Calculated current speed of car.
	 */
	private double currentSpeed;

	/**
	 * The data we got for the game.
	 */
	private GameInit gameInit;
	private double lastPos;

	/**
	 * The lane we want to switch to.
	 */
	private int targetLane;
	private int lap = 1;
	private static int lastchars;
	private int lastpieceId;
	private Piece lastpiece;

	/**
	 * Time in current piece. Used for top speed per piece calculation.
	 */
	private int piecetime;
	private int gametick;

	boolean ueberholen;

	private double lastLapTime = 999.0;

	Set<Double> avgThrottle = new HashSet<>();

	private int lastSwitchPiece;

	//	private static FileWriter filewriter, curvefilewriter;

	private TurboAvailable turboAvailable;

	private double currentInPieceDistance;

	public static void log(final String log) {
		if (!noclean) {
			System.out.println();
		}
		noclean = true;
		String newlogentry = getDateStringPrefix() + log;
		System.out.println(newlogentry);
		//		try {
		//			filewriter.append(newlogentry + '\n');
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
	}

	private static String getDateStringPrefix() {
		return df.format(new Date()) + "   ";
	}

	public static void logClean(final String log) {
		// dont output log in ci game
		if (ciGame) {
			return;
		}

		if (!noclean) {
			for (int i = 0; i < lastchars; i++) {
				System.out.print("\b");
			}
		} else {
			//			try {
			//				filewriter.append(log + '\n');
			//			} catch (IOException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}
			if (ciGame) {
				log(log);
				noclean = false;
				return;
			}
		}
		if (ciGame) {
			return;
		}
		noclean = false;
		String x = getDateStringPrefix() + log;
		lastchars = x.length();
		System.out.print(x);
		//		try {
		//			filewriter.append(x + '\n');
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
	}

	public KlausMain(final BufferedReader reader, final PrintWriter writer, final SendMsg createRace_) throws IOException {
		this.writer = writer;
		String line = null;

		if (createRace_ instanceof CreateRace) {
			CreateRace createRace = (CreateRace) createRace_;
			//			send(createRace);
			send(new JoinRace(createRace.botId.name, createRace.botId.key, createRace.trackName, createRace.password, createRace.carCount));
		} else if (createRace_ instanceof Join) {
			Join join = (Join) createRace_;
			send(join);
		}

		while ((line = reader.readLine()) != null) {
			try {
				final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

				if (msgFromServer.msgType.equals("carPositions")) {
					final CarPositions carPositions = gson.fromJson(line, CarPositions.class);
					if (carPositions.gameTick != null) {
						gametick = carPositions.gameTick;
					}
					CarPosition carPosition = null;
					enemies.clear();
					for (int i = 0; i < carPositions.data.length; i++) {
						CarPosition carPositionD = carPositions.data[i];
						if (carPositionD.id.name.equalsIgnoreCase(botName)) {
							carPosition = carPositions.data[i];
						} else {
							EnemyPos enemyPos = enemies.get(carPositionD.id.name);
							if (enemyPos == null) {
								enemyPos = new EnemyPos(carPositionD.piecePosition.lane.endLaneIndex,
														carPositionD.piecePosition.inPieceDistance,
														carPositionD.piecePosition.pieceIndex);
							}

							int laneIndex = carPositionD.piecePosition.lane.startLaneIndex;
							double currentPos =
									gameInit.data.race.track.pieces[carPositionD.piecePosition.pieceIndex].lengthFromStart[laneIndex] + carPositionD.piecePosition.inPieceDistance;

							enemyPos.currentSpeed = currentPos - enemyPos.lastPos;
							enemyPos.lastPos = currentPos;

							enemies.put(carPositionD.id.name, enemyPos);
						}
					}
					if (carPosition == null) {
						log("Error! Couldn't find car position!");
					}

					int laneIndex = carPosition.piecePosition.lane.startLaneIndex;
					currentInPieceDistance = carPosition.piecePosition.inPieceDistance;
					currentPiecePos = carPosition.piecePosition.pieceIndex;
					Piece piece = gameInit.data.race.track.pieces[currentPiecePos];

					double currentPos = piece.lengthFromStart[laneIndex] + currentInPieceDistance;
					currentSpeed = Math.max(currentPos - lastPos, 0f);
					lastPos = currentPos;

					//					if (currentSpeed <= 0.) {
					//						// we are not in the race... probably crashed
					//						// or we crossed the lap line right now
					//						System.out.print(".");
					//						send(new Throttle(.3));
					//						continue;
					//					}

					Piece nextPiece = piece.next;

					//					double negcorr = SetProperties.NEG_CORR / lap;
					//					double poscorr = SetProperties.POS_CORR / lap;

					double absAngle = Math.abs(carPosition.angle);

					//					if (absAngle > 30.) {
					//						if (diffToTargetSpeed > .3) {
					//							piece.setTargetSpeed(currentSpeed);
					//						} else {
					//							piece.addTargetSpeed(-(negcorr / (60. / absAngle)));
					//						}
					//					} else if (diffToTargetSpeed < .5) {
					//						if (diffToTargetSpeed < 0.) {
					//							piece.addTargetSpeed(-diffToTargetSpeed);
					//						} else {
					//							piece.addTargetSpeed(poscorr);
					//						}
					//					}

					//					if (currentSpeed < nextPiece.targetSpeed && currentSpeed > piece.targetSpeed) {
					//						throttle = 0.2;
					//					} else if (currentSpeed < nextPiece.targetSpeed) {
					//						throttle = 1.0;
					//					} else if (currentSpeed < piece.targetSpeed) {
					//						// current speed smaller current's piece target speed, but faster than next piece's target speed
					//						throttle = 0.5;
					//					} else {
					//						// current speed faster than current's piece target speed and faster than next piece's target speed
					//						throttle = 0.;
					//					}

					double percentInPiece = carPosition.piecePosition.inPieceDistance / piece.laneLength[laneIndex];

					// if you are almost at the end of this piece, consider the targetSpeed of the next piece
					double targetSpeed =
							piece.laneTargetSpeed[laneIndex] * (1. - percentInPiece) + nextPiece.laneTargetSpeed[laneIndex] * percentInPiece;
					double diffToTargetSpeed = targetSpeed - currentSpeed;

					if (Math.abs(piece.angle) > 1.) {
						if (currentSpeed < targetSpeed) {
							throttle = Math.max(0.1, diffToTargetSpeed / SetProperties.THROTTLE_LAZINESS);
							throttle = Math.min(1., throttle);
						} else {
							throttle = 0.;
						}
					} else {
						double diff = targetSpeed - currentSpeed;
						if (turbo && turboAvailable != null) {
							double turboFac = turboAvailable.data.turboFactor * turboAvailable.data.turboDurationTicks;
							if (diff > turboFac * SetProperties.TURBO_DIFF_FACTOR) {
								send(turboLine);
								log("Use Turbo!");
								turbo = false;
							}
						}
						if (currentSpeed < piece.laneTargetSpeed[laneIndex]) {
							throttle = 1.;
						} else {
							throttle = 0.;
						}
					}

					// if in curve, do alternative throttle computation
					//					if (Math.abs(piece.angle) > 5.) {
					//						throttle = .6 - (absAngle / 45.);
					//						throttle = Math.max(0.0, throttle);
					//					}

					if (!ueberholen) {
						for (EnemyPos ep : enemies.values()) {
							if (ep.pieceId == piece.id
								&& ep.inPieceDistance > carPosition.piecePosition.inPieceDistance
								&& ep.currentSpeed < currentSpeed
								&& ep.lane == laneIndex) {
								// we must überholen!
								//									int targetL=  FIXME TODO
								String switchLine;
								if (laneIndex > (1 - laneIndex)) {
									switchLine = "{\"msgType\": \"switchLane\", \"data\": \"Left\"}";
								} else {
									switchLine = "{\"msgType\": \"switchLane\", \"data\": \"Right\"}";
								}
								send(switchLine);
								ueberholen = true;
								continue;
							}
						}
					}
					if (nextPiece.targetLane != -1
						&& (laneIndex < nextPiece.targetLane
						|| laneIndex > nextPiece.targetLane)) {

						if (nextPiece.sw && lastSwitchPiece != piece.id) {



							log("Lane Switch from " + laneIndex + " to " + nextPiece.targetLane);
							//							send(new SwitchLane(laneIndex < nextPiece.targetLane));
							lastSwitchPiece = piece.id;
							String switchLine;
							if (laneIndex > nextPiece.targetLane) {
								switchLine = "{\"msgType\": \"switchLane\", \"data\": \"Left\"}";
							} else {
								switchLine = "{\"msgType\": \"switchLane\", \"data\": \"Right\"}";
							}
							send(switchLine);
							continue;
						}
					}

					//					throttle = Math.max(0.3, throttle);

					// for tests:
					//					if (gametick > 80) {
					//						throttle = 0.;
					//					} else {
					//						throttle = 1.;
					//					}
					//					throttle = 0.5;

					// show new line of speed if  we are on a new piece
					if (lastpieceId != piece.id) {
						ueberholen = false;
						double avgAng = 0.;
						for (CurveHistoryEntry entry : curveHistory) {
							avgAng += Math.abs(entry.angle);
						}
						avgAng = avgAng / curveHistory.size();

						//						piece.addTargetSpeed(-(1. / (60. / absAngle)));

						//						if (avgAng < 30.) {

						// recalculate top speed with average car angle
						lastpiece.laneTargetSpeed[laneIndex] += Math.max(0., ((CURVE_MAX_AVG_ANGLE - avgAng) / CURVE_MAX_AVG_ANGLE));
						gameInit.calculateMaxV();
						//						}

						//						System.out.printf("  AvgAng: %f5.2", avgAng);
						checkPieceTime(lastpiece);
						if (!ciGame) {
							System.out.println();
						}
						noclean = true;
						lastpieceId = piece.id;
						lastpiece = piece;
						avgThrottle.clear();
					}
					double avgth = 0.;
					for (Double d : avgThrottle) {
						avgth += d;
					}
					avgth = avgth / avgThrottle.size();

					// formatted output of speed
					//					String s =
					//							String
					//								.format("piece:%3d angle=%3.0f° r=%3.0f car-angle=%3.0f° throttle: %3.1f  avg-throttle: %3.1f  v-cur/target/next:%5.2f /%5.2f /%5.2f  pos: %6.1f ",
					//										piece.id, piece.angle, piece.radius, carPosition.angle, throttle, avgth, currentSpeed,
					//										piece.targetSpeed,
					//										nextPiece.targetSpeed, currentPos);
					int endLaneIndex;
					try {
						endLaneIndex = carPosition.piecePosition.lane.endLaneIndex;
					} catch (Exception ee) {
						endLaneIndex = 0;
					}
					String s =
							String
								.format("p:%3d a=%3.0f° r=%3.0f l=%3.0f car-p=%3.0f car-a=%3.0f° lane=%1d th: %3.1f  avg-th:%3.1f  v-cur/targ/next:%5.2f /%5.2f /%5.2f  pos: %6.1f ",
										piece.id, piece.angle, piece.radius, piece.laneLength[laneIndex],
										carPosition.piecePosition.inPieceDistance,
										carPosition.angle, laneIndex,
										throttle, avgth,
										currentSpeed,
										piece.laneTargetSpeed[laneIndex],
										nextPiece.targetSpeed, currentPos);
					logClean(s);

					curveHistory.add(new CurveHistoryEntry(currentSpeed, throttle, currentInPieceDistance, endLaneIndex, carPosition.angle,
															gametick - piecetime, gametick));
					avgThrottle.add(throttle);
					String string = new Gson().toJson(new KThrottle(throttle, gametick)).toString();
					//					System.out.println(string);
					send(string);

				} else if (msgFromServer.msgType.equals("crash")) {
					//					Entry entry = new Entry();
					//					entry.crashed = true;
					//					Brain.entries.put(currentPiecePos, entry);

					Piece piece = gameInit.data.race.track.pieces[currentPiecePos];
					curveLog(piece, piecetime, true);
					//					piece.addTargetSpeed(-4.);
					piece.targetSpeed -= 1.;
					piece.targetSpeed = Math.max(piece.targetSpeed, 1.);
					gameInit.calculateMaxV();

					log("Crashed at position " + currentPiecePos);

					if (!ciGame) {
						// disconnect
						reader.close();
					}

				} else if (msgFromServer.msgType.equals("join")) {
					log("Joined");
				} else if (msgFromServer.msgType.equals("gameInit")) {
					log("Race init");
					log(line);
					GameInit newinit = gson.fromJson(line, GameInit.class);
					if (gameInit == null || !newinit.data.race.track.id.equals(gameInit.data.race.track.id)) {
						gameInit = newinit;
						gameInit.init();
					}
					lastpieceId = -1;
					lastpiece = gameInit.data.race.track.pieces[0];
				} else if (msgFromServer.msgType.equals("lapFinished")) {
					log("Lap Finished");
					//					log("LAP FINISHED: " + line);
					final LapFinished lapFinished = gson.fromJson(line,
							LapFinished.class);
					double lapTimeSeconds = lapFinished.data.lapTime.millis / 1000.;
					log("lap time: " + String.format("%.3f", lapTimeSeconds));

					if (lapTimeSeconds < SetProperties.FASTEST_LAP_TIME) {
						SetProperties.FASTEST_LAP_TIME = lapTimeSeconds;
						SetProperties.save();
						log(" **** New fastest lap!!! **** ");
					} else if (lapTimeSeconds > SetProperties.FASTEST_LAP_TIME * 1.5
					/*|| lapTimeSeconds > lastLapTime*/) {
						// slower than double the fastest time... disconnect!
						if (!ciGame) {
							reader.close();
						}
					}
					lastLapTime = lapTimeSeconds;

					lap++;

				} else if (msgFromServer.msgType.equals("dnf")) {
					log("DNF");
				} else if (msgFromServer.msgType.equals("gameEnd")) {
					log("Race end");
				} else if (msgFromServer.msgType.equals("gameStart")) {
					log("Race start");
					send(new Gson().toJson(new KThrottle(1.0, 0)).toString());

				} else if (msgFromServer.msgType.equals("turboAvailable")) {
					log("Turbo Available!");
					turboAvailable = gson.fromJson(line, TurboAvailable.class);
					turbo = true;
				} else {
					log("OTHER MESSAGE: " + line);
					send(new Ping());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static final double PARAM_THROTTLE = 0.215;
	public static final double PARAM_ACCEL = 0.021;

	private static String botName;

	public double getVnext() {
		return getVnext(currentSpeed, throttle);
	}

	public static double getVprev(final double currentV, final double throttle) {
		return throttle * -PARAM_THROTTLE + (1. + PARAM_ACCEL) * currentV;
	}

	public static double getVnext(final double currentV, final double throttle) {
		return throttle * PARAM_THROTTLE + (1. - PARAM_ACCEL) * currentV;
	}

	/**
	 * called on piece change.
	 */
	private void checkPieceTime(final Piece piece) {
		int diff = gametick - piecetime;
		piecetime = gametick;
		//		if (piece != null && Math.abs(piece.angle) > 1.) {
		if (piece != null) {
			//			curveLog(piece, diff, false);
		}
		curveHistory.clear();
	}

	private void curveLog(final Piece piece, final int piecetime, final boolean crashed) {
		//		if (Math.abs(piece.angle) > 1.)
		{
			CurveKey key = new CurveKey();
			Integer cv;
			if (Math.abs(piece.angle) > 1.) {
				key.angle = piece.angle;
				key.radius = piece.radius;
				key.crashed = crashed;
				cv = CurveProps.getCurveValues().get(key.toString());
			} else {
				cv = 0;
				key.angle = 0.;
				key.radius = piece.length;
				key.crashed = crashed;
			}
			//			if (cv == null || piecetime < cv || crashed) 
			{
				//				CurveProps.getCurveValues().put(key.toString(), piecetime);
				//				CurveProps.save();

				//				try {
				//					String SEP = ";";
				//					long currentTimeMillis = System.currentTimeMillis();
				//					for (CurveHistoryEntry entry : curveHistory) {
				//						String s =
				//								currentTimeMillis + SEP + Math.abs(piece.angle) + SEP + piece.radius + SEP + (crashed ? "1" : "0")
				//										+ SEP + entry.position + SEP + entry.inPieceGametick
				//										+ SEP + entry.currentSpeed + SEP + entry.throttle
				//										+ SEP + entry.angle + SEP + entry.lane + SEP + entry.gametick;
				//											curvefilewriter.append(s + '\n');
				//					}
				//					curveHistory.clear();
				//				} catch (IOException e) {
				//					// TODO Auto-generated catch block
				//					e.printStackTrace();
				//				}
			}
		}
	}

	private void send(final String s) {
		writer.println(s);
		writer.flush();
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}
