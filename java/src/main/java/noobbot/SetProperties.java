package noobbot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;

public class SetProperties {

	public static double FASTEST_LAP_TIME = 20.;
	/**
	 * The higher the value, the slower the throttle reacts.
	 */
	//	public static double THROTTLE_LAZINESS = 2.;
	//	public static double TURBO_DIFF_FACTOR = .3;
	//	public static double CURVE_V_INIT_PARAM = 9.3;o
	//	public static double CURVE_V_INIT_PARAM2 = 0.64;
	//	public static double CURVE_V_INIT_PARAM3 = 4.;
	// finland:
	public static double THROTTLE_LAZINESS = 1.9342379345996144;
	public static double TURBO_DIFF_FACTOR = 0.35;//;0.17465682854718062;
	public static double CURVE_V_INIT_PARAM = 7.; //6.75;
	public static double CURVE_V_INIT_PARAM2 = 0.465; //0.46; //5378963321045724;
	public static double CURVE_V_INIT_PARAM3 = 4.;

	/**
	 * the higher, the less pieces in the distance matter
	 */
	//	public static double DISTANCE_RISK_MINIFICATION_FACTOR = .45;
	//	public static double MAX_DISTANCE_FOR_RISK_CALCULATION = 270.;
	//	public static double MAX_VELOCITY = 26.22;
	//	public static double MIN_VELOCITY = 2.93;
	//	public static double CURVE_RISK_FACTOR = 1.;//.8;
	//	public static double POS_CORR = .06;
	//	public static double NEG_CORR = .08;

	// do not make following fields public! or else save() will try to put them into the properties file.
	private static String mapname;

	private static Random rand = new Random(System.currentTimeMillis());

	public static void setMapname(final String mapname) {
		SetProperties.mapname = mapname + ".properties";
	}

	public static void load() {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(new File(mapname)));

			for (Entry entry : p.entrySet()) {
				try {

					String fieldname = entry.getKey().toString();
					Field field = SetProperties.class.getField(fieldname);
					double value = Double.parseDouble(entry.getValue().toString());

					// make random
					//					if (!entry.getKey().equals("FASTEST_LAP_TIME")) {
					//						value = value + ((rand.nextDouble() - .5) * (value / 2.));
					//					}
					//					field.set(null, value);

					KlausMain.log(entry.getKey() + " = " + value);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			Thread.sleep(1); // 1000

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void save() {
		Properties p = new Properties();

		Field[] fields = SetProperties.class.getFields();
		for (Field field : fields) {
			try {
				p.put(field.getName(), Double.toString(field.getDouble(null)));
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			p.store(new FileWriter(new File(mapname)), "");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
